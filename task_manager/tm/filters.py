import django_filters
from .models import Task, STATUS_CHOICES, PRIORITY_CHOICES
from django.utils import timezone
from django import forms


class IndexFilter(django_filters.FilterSet):
    class Meta:
        model = Task
        exclude = ['title', 'description', 'deadline']

    with_my_comment = django_filters.BooleanFilter(field_name='comment__author', method='is_user')
    my_tasks = django_filters.BooleanFilter(field_name='author', method='is_user')
    me_executor = django_filters.BooleanFilter(field_name='executor', method='is_user')
    over_due = django_filters.BooleanFilter(field_name='deadline', method='is_over_due')
    status = django_filters.ChoiceFilter(choices=STATUS_CHOICES, widget=forms.Select(attrs={'class': 'form-control',}))
    priority = django_filters.ChoiceFilter(choices=PRIORITY_CHOICES, widget=forms.Select(attrs={'class': 'form-control', }))

    def is_over_due(self, qs, name, value):
        return qs.filter(deadline__lt=timezone.now(), status=0)

    def is_user(self, qs, name, value):
        if self.request.user.is_authenticated:
            return qs.filter(**{name: self.request.user.id})
        return qs
