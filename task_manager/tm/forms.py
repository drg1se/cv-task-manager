from django import forms
from datetime import datetime
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from registration.forms import RegistrationForm
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from .models import Comment


class ChangeCommentForm(forms.Form):
    comment_edit_text = forms.CharField(max_length=500, required=True)
    cid = forms.IntegerField


class CommentForm(forms.ModelForm):
    text = forms.CharField(max_length=500, required=True, widget=forms.Textarea(attrs={
        'class': 'form-control',
        'id': 'comment_text_form',
    }))

    class Meta:
        model = Comment
        fields = ['text']


class LoginForm(AuthenticationForm):
    username = forms.CharField(max_length=150, label=_('Username'), required=True,
                               widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(max_length=100, label=_('Password'), required=True,
                               widget=forms.TextInput(attrs={'class': 'form-control',
                                                             'name': "password1",
                                                             'type': "password"}))


class MyRegistrationForm(RegistrationForm):
    username = forms.CharField(max_length=150, label=_('Username'), required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    first_name = forms.CharField(max_length=30, label=_('First name'), required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(max_length=150, label=_('Last name'), required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(max_length=254, widget=forms.TextInput(attrs={'class': 'form-control'}))
    password1 = forms.CharField(max_length=100, label=_('Password'), required=True,
                                widget=forms.TextInput(attrs={'class': 'form-control',
                                                              'name': "password1",
                                                              'type': "password"}))
    password2 = forms.CharField(max_length=100, label=_('Repeat your password'), required=True,
                                widget=forms.TextInput(attrs={'class': 'form-control',
                                                              'name': "password2",
                                                              'type': "password"}))

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username']
        widgets = {'username': forms.TextInput(attrs={'class': 'form-control'})}


class NewTaskForm(forms.Form):
    title = forms.CharField(label='Title: ', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control'}))
    deadline_date = forms.DateField(widget=forms.DateInput(attrs={'class': 'form-control'}))
    deadline_time = forms.TimeField(widget=forms.TimeInput(attrs={'class': 'form-control'}))
    priority = forms.ChoiceField(label='Priority: ', choices=[
        (0, 'Low'),
        (1, 'Medium'),
        (2, 'High'),
    ], widget=forms.Select(attrs={'class': 'form-control'}))
    description = forms.CharField(label='Description: ', max_length=500, widget=forms.Textarea(attrs={
        'class': 'form-control'
    }))
    executor = forms.ChoiceField(label='Executor: ', choices=[
        (user.id, user.get_full_name()) for user in User.objects.all() if user.id > 1
    ], widget=forms.Select(attrs={'class': 'form-control'}))
