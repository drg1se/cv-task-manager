from django.db import models
from django.contrib.auth.models import User
import django_filters

STATUS_CHOICES = {
    (0, 'Open'),
    (1, 'Pending for approval'),
    (2, 'Closed'),
}

PRIORITY_CHOICES = {
    (0, 'Low'),
    (1, 'Medium'),
    (2, 'High')
}

class Task(models.Model):
    deadline = models.DateTimeField('DeadLine')
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=500)
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='mytask')
    executor = models.ForeignKey(User, on_delete=models.CASCADE, related_name='task')
    priority = models.IntegerField(choices=PRIORITY_CHOICES, default=1)  # 0 - low, 1 - medium, 2 - high
    status = models.IntegerField(choices=STATUS_CHOICES, default=0)  # 0 - open, 1 - pending for approval, 2 - closed

    def __str__(self):
        return '\n'.join([
            'Task#{} {}'.format(self.id, self.title),
            'Deadline: {}'.format(self.deadline),
            ('Closed' if self.status == 2 else 'Pending for approval' if self.status else 'Open'),
        ])


class Comment(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='comment')
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comment')
    published = models.DateTimeField('published')
    text = models.CharField(max_length=500)

    def __str__(self):
        return "Comment#{} to {} by {}".format(str(self.id), self.task.title, self.author.get_full_name())

