from django.urls import path
from django.contrib.auth.views import LoginView
from registration.backends.simple.views import RegistrationView
from . import views
from . import forms

urlpatterns = [
    path(r'signup/', RegistrationView.as_view(form_class=forms.MyRegistrationForm), name='register'),
    path(r'login/', LoginView.as_view(form_class=forms.LoginForm), name='login'),
    path(r'logout/', views.mylogout, name='logout'),
    path(r'change_task/', views.change_task, name='change_task'),
    path(r'new_task/', views.new_task, name='new_task'),
    path(r'', views.IndexView.as_view(), name='index'),
    path(r'task/<int:id>/', views.task, name='task'),
]