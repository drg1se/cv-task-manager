from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404, HttpResponseForbidden, HttpResponseBadRequest
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import logout
from django.utils.dateparse import parse_date, parse_time
from .models import Task, Comment
from datetime import datetime
from .forms import NewTaskForm, CommentForm, ChangeCommentForm
from django.utils.timezone import now
from django_filters.views import FilterView
from .filters import IndexFilter


class IndexView(FilterView):
    template_name = 'tm/index.html'
    model = Task
    paginate_by = 2
    filterset_class = IndexFilter


def mylogout(req):
    logout(req)
    return redirect('/login')


@login_required
def change_task(req):
    if req.method == 'GET':
        return HttpResponseForbidden('Only POST methods are accepted')
    task = None
    try:
        task = get_object_or_404(Task, id=int(req.POST['id']))
    except KeyError:
        return HttpResponseBadRequest('Necessary argument \"id\" not passed')
    if req.POST['submit'] == 'delete':
        if req.user == task.author:
            task.delete()
            return redirect('/')
        else:
            return HttpResponseForbidden('Only author can delete the task')
    if req.POST['submit'] == 'change':
        if req.user == task.author:
            task.description = req.POST['description']
            task.executor = User.objects.get(id=int(req.POST['executor']))
            task.priority = int(req.POST['priority'])
            task.deadline = datetime.combine(parse_date(req.POST['deadline_date']), parse_time(req.POST['deadline_time']))
        if req.user == task.author or req.user == task.executor:
            task.status = int(req.POST['status'])
        task.save()
        return redirect('/task/{}'.format(req.POST['id']))
    return HttpResponseBadRequest('Invalid submit parameter')


@login_required
def new_task(req):
    if req.method == "POST":
        form = NewTaskForm(req.POST)
        if form.is_valid():
            id = Task.objects.create(
                title=form.cleaned_data['title'],
                description=form.cleaned_data['description'],
                author=req.user,
                executor=User.objects.get(id=form.cleaned_data['executor']),
                priority=form.cleaned_data['priority'],
                status=0,
                deadline=datetime.combine(form.cleaned_data['deadline_date'], form.cleaned_data['deadline_time'])
            ).id
            return redirect('/task/{}/'.format(id))
        return HttpResponseBadRequest('Invalid request passed')
    elif req.method == 'GET':
        return render(req, 'tm/new_task.html', {'form': NewTaskForm({'executor': req.user.id, 'priority': 1})})


@login_required
def task(req, id):
    task = get_object_or_404(Task, id=id)
    if req.method == 'POST':
        if req.POST['submit'] == 'comment':
            form = CommentForm(req.POST)
            if form.is_valid():
                comment = form.save(commit=False)
                comment.author = req.user
                comment.task = task
                comment.published = now()
                comment.save()
        elif req.POST['submit'] == 'edit_comment':
            form = ChangeCommentForm(req.POST)
            if form.is_valid():
                comment = get_object_or_404(Comment, id=req.POST['cid'])
                comment.text = req.POST['comment_edit_text']
                comment.save()
        elif req.POST['submit'] == 'delete_comment':
            get_object_or_404(Comment, id=req.POST['cid']).delete()

        return redirect('/task/' + str(task.id))

    return render(req, 'tm/task.html', {
        'task': task,
        'priority': 'High' if task.priority == 2 else 'Medium' if task.priority else 'Low',
        'status': ('Closed' if task.status == 2 else 'Pending for approval' if task.status else 'Open'),
        'author': task.author,
        'executor': task.executor,
        'executors': User.objects.all(),
        'comment_form': CommentForm,
        'change_comment_form': ChangeCommentForm,
        'comments': task.comment.extra(order_by=['-published'])
    })

